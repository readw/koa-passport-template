const Router = require("koa-router");
const Logger = require("../Common/Logger");

class ProfileController {
    constructor(connection) 
    {
        this.connection = connection;
        this.router = new Router();
        this.logger = new Logger("ProfileController");

        this.authenticationMethods = AuthenticationMethods();

        this.InitialiseEndpoints();
    }

    InitialiseEndpoints()
    {
        if (this.authenticationMethods.length > 0) {
            this.router.get("/:user_id", async (ctx) => {
                ctx.status = 200;
                await ctx.render("/Profile/Index", {user: ctx.state.user });
            });
        }
    }
}
 
module.exports = (connection) => {
    var profileController = new ProfileController(connection);
    return profileController.router.routes();
};