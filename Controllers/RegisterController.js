const Router = require("koa-router");
const Passport = require("koa-passport");
const Logger = require("../Common/Logger");

class RegisterController {
    constructor(connection) 
    {
        this.connection = connection;
        this.router = new Router();
        this.logger = new Logger("RegisterController");

        this.InitialiseEndpoints();
    }

    InitialiseEndpoints()
    {
        if (process.env.IS_LOCAL_AUTHENTICATION == 1) {
            this.router.get("/", async (ctx) => {
                ctx.status = 200;
                await ctx.render("/Account/Register", {errors: []});
            });
    
            this.router.post("/", (ctx) => {
                return Passport.authenticate("register", async (err, user) => {
                    if (user) { 
                        ctx.status = 302;
                        this.logger.Debug(user);
                        ctx.login(user._id);
                        ctx.redirect("/Home");
                    } else {
                        ctx.status = 401;
                        this.logger.Error(err);
                        await ctx.render("/Account/Register", {errors: err ? [err] : [`${ctx.status} - ${ctx.message}`]});
                    }
                })(ctx);
            });
        }
    }
}
 
module.exports = (connection) => {
    var registerController = new RegisterController(connection);
    return registerController.router.routes();
};