# Koa-Passport Template

Simplistic Authentication Application using the following popular JS Frameworks:
Koa JS & Passport

### .env Configuration
| Environment Variable       |   Default Value   |     Alternative Values    |
|----------------------------|-------------------|---------------------------|
| NODE_ENV                   | development       | development or production |
| PORT                       | 4000              | Any integer value         |
| SECRET                     | secret            | Any string                |
| IS_TEST_MODE               | false             | true or false             |
| MONGO_DB_URL               |                   | Any mongodb url           |
| GOOGLE_CLIENT_ID           | default           | Valid Google Id           |
| GOOGLE_CLIENT_SECRET       | default           | Valid Google Secret       |
| FACEBOOK_CLIENT_ID         | default           | Valid Facebook Id         |
| FACEBOOK_CLIENT_SECRET     | default           | Valid Facebook Secret     |
| TWITTER_CLIENT_ID          | default           | Valid Twitter Id          |
| TWITTER_CLIENT_SECRET      | default           | Valid Twitter Secret      |
| IS_LOCAL_AUTHENTICATION    | 0                 | 0 or 1                    |
| IS_FACEBOOK_AUTHENTICATION | 0                 | 0 or 1                    |
| IS_GOOGLE_AUTHENTICATION   | 0                 | 0 or 1                    |
| IS_TWITTER_AUTHENTICATION  | 0                 | 0 or 1                    |