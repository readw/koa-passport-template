AuthenticationMethods = () => {
    var authenticationMethods = [];

    if (process.env.IS_LOCAL_AUTHENTICATION == 1) {
        authenticationMethods.push("local");
    }
    if (process.env.IS_FACEBOOK_AUTHENTICATION == 1) {
        authenticationMethods.push("facebook");
    }
    if (process.env.IS_GOOGLE_AUTHENTICATION == 1) {
        authenticationMethods.push("google");
    }
    if (process.env.IS_TWITTER_AUTHENTICATION == 1) {
        authenticationMethods.push("twitter");
    }

    return authenticationMethods;
}

module.exports = {
    AuthenticationMethods
};