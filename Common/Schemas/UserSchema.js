const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

class UserSchema {

    constructor()
    {
        let UserSchema = mongoose.Schema({
            username: String,
            password: String,
            display_name: String,
            email: String,
            local_id: String,
            facebook_id: String,
            google_id: String,
            twitter_id: String
        }, {
            versionKey: false
        });

        UserSchema.methods.generateHash = (password) => this._generateHash(password);
        UserSchema.methods.validPassword = (password, hash) => this._validPassword(password, hash);
        UserSchema.methods.generateJwt = (user) => this._generateJwt(user);

        // Determine if model exists.
        try {
            return mongoose.model("Users");
        } catch {
            return mongoose.model("Users", UserSchema);
        }
    }

    _generateHash(password)
    {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
    }

    _validPassword(password, hash)
    {
        return bcrypt.compareSync(password, hash);
    }

    _generateJwt(user)
    {
        return jwt.sign({
            user: {
                _id: user._id,
                displayName: user.display_name,
                image: user.profile_picture
            }
        }, process.env.SECRET, { expiresIn: "86400s", issuer: "koa-passport" });
    }
}

module.exports = UserSchema;